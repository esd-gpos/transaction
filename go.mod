module gitlab.com/esd-gpos/transaction

go 1.13

require (
	github.com/gin-contrib/cors v1.3.1 // indirect
	github.com/gin-gonic/gin v1.6.2 // indirect
	github.com/golang/protobuf v1.4.0 // indirect
	github.com/google/uuid v1.1.1 // indirect
	github.com/klauspost/compress v1.10.5 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	gitlab.com/esd-gpos/auth v0.0.0-20200330100020-24e8e06e3103 // indirect
	go.mongodb.org/mongo-driver v1.3.2 // indirect
	golang.org/x/crypto v0.0.0-20200422194213-44a606286825 // indirect
	golang.org/x/sync v0.0.0-20200317015054-43a5402ce75a // indirect
	golang.org/x/sys v0.0.0-20200420163511-1957bb5e6d1f // indirect
)
