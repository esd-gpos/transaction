package transaction

import (
	"context"
	"errors"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"gitlab.com/esd-gpos/auth/middleware"
	"net/http"
	"time"
)

func RegisterRoutes(h *Handler, engine *gin.Engine, auth *middleware.Middleware, mid *Middleware) {
	// Register routes for web client
	web := engine.Group("api/v1/transaction")
	web.Use(auth.GinHandler)
	web.Use(mid.CheckUserId)

	web.GET("", h.ListStoreTransactions)
	web.GET(":transactionId", h.GetTransaction)

	// Register routes for mobile client
	mobile := engine.Group("mobile/api/v1/transaction")
	mobile.Use(mid.CheckToken)

	mobile.POST("", h.CreateTransaction)
	mobile.GET("", h.ListStoreTransactions)
	mobile.GET(":transactionId", h.GetTransaction)
}

// Contains entity and sub entity types
type Entity struct {
	TransactionId string  `json:"transaction_id" bson:"_id" binding:"omitempty,uuid4"`
	UserId        string  `json:"-" bson:"user_id" binding:"omitempty,uuid4"`
	StoreId       string  `json:"store_id" bson:"store_id" binding:"omitempty,uuid4"`
	Items         []Item  `json:"items" bson:"items" binding:"required,dive"`
	Price         float32 `json:"price" bson:"price" binding:"gte=0"`
	Timestamp     string  `json:"timestamp" bson:"timestamp"`
}

type Item struct {
	ItemId    string `json:"item_id" bson:"item_id" binding:"uuid4"`
	ProductId string `json:"product_id" bson:"product_id" binding:"omitempty,uuid4"`
	Quantity  int    `json:"quantity" bson:"quantity" binding:"gte=1"`
}

// Contains Repo for interacting with transactions data
var errTransNotFound = errors.New("transaction not found")

type Repo interface {
	CreateTransaction(ctx context.Context, entity *Entity) error

	ListStoreTransactions(ctx context.Context, userId string, storeId string) ([]*Entity, error)

	GetTransaction(ctx context.Context, entity *Entity) error
}

// Contains Repo for interacting with Store-Inventory data
var errItemNotFound = errors.New("item not found")

type ItemRepo interface {
	// Checks if items for this transaction exist in the store
	CheckItems(ctx context.Context, entity *Entity) error
}

// Handler type for Transaction endpoint
const (
	resCreateTran     = "transaction created"
	resListStoreTrans = "list of transactions for store retrieved"
	resGetTransaction = "transaction retrieved"

	resInvalid      = "invalid format"
	resInternal     = "internal error"
	resItemNotFound = "one of the item does not exist in store"
	resTranNotFound = "transaction not found"
)

type Handler struct {
	Repo     Repo
	ItemRepo ItemRepo
}

func (h *Handler) CreateTransaction(ctx *gin.Context) {
	userId := ctx.GetString("userId")
	storeId := ctx.GetString("storeId")

	// Bind body
	entity := &Entity{}
	if err := ctx.ShouldBindJSON(entity); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"message": resInvalid})
		return
	}

	entity.UserId = userId
	entity.StoreId = storeId
	entity.TransactionId = uuid.New().String()
	entity.Timestamp = time.Now().String()

	// Validate item
	if err := h.ItemRepo.CheckItems(ctx, entity); err != nil {
		if err == errItemNotFound {
			ctx.JSON(http.StatusNotFound, gin.H{"message": resItemNotFound})
		} else {
			ctx.JSON(http.StatusInternalServerError, gin.H{"message": resInternal})
		}

		return
	}

	// Create Transaction
	if err := h.Repo.CreateTransaction(ctx, entity); err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{"message": resInternal})
		return
	}

	ctx.JSON(http.StatusAccepted, gin.H{"message": resCreateTran})
}

func (h *Handler) ListStoreTransactions(ctx *gin.Context) {
	userId := ctx.GetString("userId")
	storeId := ctx.GetString("storeId")

	entities, err := h.Repo.ListStoreTransactions(ctx, userId, storeId)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{"message": resInternal})
		return
	}

	ctx.JSON(http.StatusOK, gin.H{"message": resListStoreTrans, "result": entities})
}

func (h *Handler) GetTransaction(ctx *gin.Context) {
	userId := ctx.GetString("userId")
	storeId := ctx.GetString("storeId")

	transactionId := ctx.Param("transactionId")
	if _, err := uuid.Parse(transactionId); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"message": resInvalid})
		return
	}

	entity := &Entity{
		TransactionId: transactionId,
		UserId:        userId,
		StoreId:       storeId,
	}

	if err := h.Repo.GetTransaction(ctx, entity); err != nil {
		if err == errTransNotFound {
			ctx.JSON(http.StatusNotFound, gin.H{"message": resTranNotFound})
		} else {
			ctx.JSON(http.StatusInternalServerError, gin.H{"message": resInternal})
		}

		return
	}

	ctx.JSON(http.StatusOK, gin.H{"message": resGetTransaction, "result": entity})
}
