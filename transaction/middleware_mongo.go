package transaction

import (
	"context"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

type MongoStoreRepo struct {
	Col *mongo.Collection
}

func (m *MongoStoreRepo) CheckToken(ctx context.Context, storeId string, storeToken string) (string, error) {
	res := m.Col.FindOne(ctx, bson.M{"_id": storeId, "token": storeToken})
	if res.Err() != nil {
		if res.Err() == mongo.ErrNoDocuments {
			return "", errStoreNotFound
		}

		return "", res.Err()
	}

	resBody := &MongoStoreBody{}
	if err := res.Decode(resBody); err != nil {
		return "", err
	}

	return resBody.UserId, nil
}

func (m *MongoStoreRepo) CheckUserId(ctx context.Context, storeId string, userId string) error {
	res := m.Col.FindOne(ctx, bson.M{"_id": storeId, "user_id": userId})
	if res.Err() != nil {
		if res.Err() == mongo.ErrNoDocuments {
			return errStoreNotFound
		}

		return res.Err()
	}

	return nil
}

type MongoStoreBody struct {
	StoreId string `bson:"_id"`
	UserId  string `bson:"user_id"`
	Token   string `bson:"token"`
}
