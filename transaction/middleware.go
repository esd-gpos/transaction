package transaction

import (
	"context"
	"errors"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"log"
	"net/http"
)

// Will be used for middleware tha interacts with store data to verify token
var errStoreNotFound = errors.New("store not found")

type StoreRepo interface {
	CheckToken(ctx context.Context, storeId string, storeToken string) (string, error)

	CheckUserId(ctx context.Context, storeId string, userId string) error
}

// Middleware handler
const resStoreNotFound = "could not find store"

type Middleware struct {
	StoreRepo StoreRepo
}

// This is used for mobile routes
func (m *Middleware) CheckToken(ctx *gin.Context) {
	log.Println(ctx.Request.URL)

	storeId := ctx.Query("storeId")
	if storeId == "" {
		ctx.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"message": resInvalid, "err": "empty storeId"})
		return
	} else if _, err := uuid.Parse(storeId); err != nil {
		ctx.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"message": resInvalid, "err": "uuid parse storeId error", "val": storeId})
		return
	}

	storeToken := ctx.GetHeader("store-token")
	if storeToken == "" {
		ctx.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"message": resInvalid, "err": "empty store-token"})
		return
	} else if _, err := uuid.Parse(storeToken); err != nil {
		ctx.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"message": resInvalid, "err": "uuid parse store-token error"})
		return
	}

	userId, err := m.StoreRepo.CheckToken(ctx, storeId, storeToken)
	if err != nil {
		if err == errStoreNotFound {
			ctx.AbortWithStatusJSON(http.StatusNotFound, gin.H{"message": resStoreNotFound})
		} else {
			ctx.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"message": resInternal})
		}

		return
	}

	ctx.Set("userId", userId)
	ctx.Set("storeId", storeId)
}

// This is used for web routes after the auth middleware
func (m *Middleware) CheckUserId(ctx *gin.Context) {
	userId := ctx.GetString("userId")

	storeId := ctx.Query("storeId")
	if storeId == "" {
		ctx.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"message": resInvalid})
		return
	} else if _, err := uuid.Parse(storeId); err != nil {
		ctx.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"message": resInvalid})
		return
	}

	if err := m.StoreRepo.CheckUserId(ctx, storeId, userId); err != nil {
		if err == errStoreNotFound {
			ctx.AbortWithStatusJSON(http.StatusNotFound, gin.H{"message": resStoreNotFound})
		} else {
			ctx.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"message": resInternal})
		}

		return
	}

	ctx.Set("storeId", storeId)
}
