package transaction

import (
	"context"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

type MongoItemRepo struct {
	Col *mongo.Collection
}

func (m *MongoItemRepo) CheckItems(ctx context.Context, entity *Entity) error {
	for i, item := range entity.Items {
		if productId, err := m.checkItem(ctx, item.ItemId, entity.StoreId, entity.UserId); err != nil {
			return err
		} else {
			entity.Items[i].ProductId = productId
		}
	}

	return nil
}

func (m *MongoItemRepo) checkItem(ctx context.Context, itemId string, storeId string, userId string) (string, error) {
	res := m.Col.FindOne(ctx, bson.M{"_id": itemId, "store_id": storeId, "user_id": userId})
	if res.Err() != nil {
		if res.Err() == mongo.ErrNoDocuments {
			return "", errItemNotFound
		}

		return "", res.Err()
	}

	resBody := &MongoItemBody{}
	if err := res.Decode(resBody); err != nil {
		return "", err
	}

	return resBody.ProductId, nil
}

type MongoItemBody struct {
	ProductId string `bson:"product_id"`
}
