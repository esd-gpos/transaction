package transaction

import (
	"context"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

type MongoRepo struct {
	Col *mongo.Collection
}

func (m *MongoRepo) CreateTransaction(ctx context.Context, entity *Entity) error {
	if _, err := m.Col.InsertOne(ctx, entity); err != nil {
		return err
	}

	return nil
}

func (m *MongoRepo) ListStoreTransactions(ctx context.Context, userId string, storeId string) ([]*Entity, error) {
	cursor, err := m.Col.Find(ctx, bson.M{"user_id": userId, "store_id": storeId})
	if err != nil {
		return nil, err
	}

	entities := make([]*Entity, 0)

	for cursor.Next(ctx) {
		resBody := &Entity{}
		if err = cursor.Decode(resBody); err != nil {
			return nil, err
		}

		entities = append(entities, resBody)
	}

	return entities, nil

}

func (m *MongoRepo) GetTransaction(ctx context.Context, entity *Entity) error {
	res := m.Col.FindOne(ctx, bson.M{"_id": entity.TransactionId, "store_id": entity.StoreId})
	if res.Err() != nil {
		if res.Err() == mongo.ErrNoDocuments {
			return errTransNotFound
		}

		return res.Err()
	}

	if err := res.Decode(entity); err != nil {
		return err
	}

	return nil
}
