package main

import (
	"context"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"gitlab.com/esd-gpos/auth/middleware"
	"gitlab.com/esd-gpos/transaction/transaction"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
	"net/http"
	"os"
	"strings"
	"time"
)

func main() {
	// Get configurations
	mongoUri := os.Getenv("MONGO_URI")
	mongoDb := os.Getenv("MONGO_DB")

	audience := os.Getenv("AUDIENCE")
	issuer := os.Getenv("ISSUER")
	publicKey := os.Getenv("PUBLIC_KEY")

	failOnEmpty(mongoUri, mongoDb, audience, issuer, publicKey)

	// Setup Mongo
	mongoClient, err := mongo.NewClient(options.Client().ApplyURI(mongoUri))
	failOnError("could not create mongo client", err)

	timeout, _ := context.WithTimeout(context.Background(), time.Second*30)

	err = mongoClient.Connect(timeout)
	failOnError("could not create connection with mongo client", err)

	timeout.Done()

	mongoDatabase := mongoClient.Database(mongoDb)
	storeCollection := mongoDatabase.Collection("store")
	itemCollection := mongoDatabase.Collection("item")
	transactionCollection := mongoDatabase.Collection("transaction")

	// Setup Gin
	corsConfig := cors.Config{
		AllowAllOrigins:  true,
		AllowMethods:     []string{"GET", "POST", "PUT", "PATCH", "DELETE", "HEAD"},
		AllowHeaders:     []string{"Origin", "Content-Length", "Content-Type", "Authorization"},
		AllowCredentials: false,
		MaxAge:           12 * time.Hour,
	}

	engine := gin.New()
	engine.Use(cors.New(corsConfig))

	// Setup Middleware
	authInfo := &middleware.Information{
		Audience:  audience,
		Issuer:    issuer,
		PublicKey: publicKey,
	}

	authMiddleware, err := middleware.NewMiddleware(authInfo)
	failOnError("could not create auth middleware", err)

	// Setup Transaction endpoint
	transStoreRepo := &transaction.MongoStoreRepo{Col: storeCollection}
	transMiddleware := &transaction.Middleware{StoreRepo: transStoreRepo}

	transItemRepo := &transaction.MongoItemRepo{Col: itemCollection}
	transRepo := &transaction.MongoRepo{Col: transactionCollection}
	transHandler := &transaction.Handler{
		Repo:     transRepo,
		ItemRepo: transItemRepo,
	}

	transaction.RegisterRoutes(transHandler, engine, authMiddleware, transMiddleware)

	// Start program
	engine.GET("ping", func(ctx *gin.Context) {
		if err := mongoClient.Ping(ctx, nil); err != nil {
			ctx.JSON(http.StatusInternalServerError, gin.H{"message": "not ready"})
		} else {
			ctx.JSON(http.StatusOK, gin.H{"message": "ready"})
		}
	})

	log.Fatal(engine.Run("0.0.0.0:7600"))
}

func failOnEmpty(str ...string) {
	for _, s := range str {
		if strings.TrimSpace(s) == "" {
			log.Fatal("missing env variable")
		}
	}
}

func failOnError(msg string, err error) {
	if err != nil {
		log.Fatalf("%s:%s", msg, err)
	}
}
